from django.test import TestCase, Client
from django.urls import resolve

# Create your tests here.
class Story8UnitTest(TestCase):

	def test_story_8_url_is_exist(self):
		response = Client().get('/s8/')
		self.assertEqual(response.status_code, 200)

	def test_using_profile_template(self):
		response = Client().get('/s8/')
		self.assertTemplateUsed(response, 'about.html')

	def test_profile_contains_name(self):
		response = Client().get('/s8/')
		response_content = response.content.decode('UTF-8')
		self.assertIn('Yusuf Tri Ardho', response_content)