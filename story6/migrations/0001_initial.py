# Generated by Django 2.1.1 on 2018-10-10 22:04

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(default=datetime.datetime(2018, 10, 10, 22, 4, 13, 617595, tzinfo=utc))),
                ('content', models.TextField(max_length=300)),
            ],
        ),
    ]
