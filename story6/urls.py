from django.urls import path
from . import views

app_name = 's6'

urlpatterns = [
    path('', views.index),
    path('profile/', views.profile)
]