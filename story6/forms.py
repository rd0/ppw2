from django import forms
from .models import Status

class StatusForm(forms.Form):
	content = forms.CharField(label='', max_length=300, widget=forms.TextInput(attrs={
		'class': 'input',
		'placeholder': 'type your status ...',
		'required': True,
		'autocomplete': 'off',
	}))
