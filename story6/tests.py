from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from .models import Status
from .views import index, profile
from .forms import StatusForm
import time
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class Story6UnitTest(TestCase):

	# Test Story 6
	def test_story_6_url_is_exist(self):
		response = Client().get('/s6/')
		self.assertEqual(response.status_code, 200)

	def test_using_index_func(self):
		found = resolve('/s6/')
		self.assertEqual(found.func, index)

	def test_using_index_template(self):
		response = Client().get('/s6/')
		self.assertTemplateUsed(response, 'index.html')

	def test_index_contains_greeting(self):
		response = Client().get('/s6/')
		response_content = response.content.decode('UTF-8')
		self.assertIn('Hello, Apa Kabar ?', response_content)

	def test_model_can_create_new_status(self):
		Status.objects.create(date=timezone.now(), content='<s3bu@h "tEst" >,<')
		counting_all_available_status = Status.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)

	def test_form_validation_for_blank_items(self):
		form = StatusForm(data={'date': '', 'content': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
		    form.errors['content'],
		    ['This field is required.']
		)

	def test_story_6_post_success_and_render_the_result(self):
		# s = '<s3bu@h "tEst" >,<'
		s = 'b1@s4_aj4 !~'
		response_post = Client().post('/s6/', {'date': timezone.now(), 'content': s})
		self.assertEqual(response_post.status_code, 302)

		response = Client().get('/s6/')
		html_response = response.content.decode('UTF-8')
		self.assertIn(s, html_response)

	# Test Challenge 6
	def test_challenge_6_url_is_exist(self):
		response = Client().get('/s6/profile/')
		self.assertEqual(response.status_code, 200)

	def test_using_profile_func(self):
		found = resolve('/s6/profile/')
		self.assertEqual(found.func, profile)

	def test_using_profile_template(self):
		response = Client().get('/s6/profile/')
		self.assertTemplateUsed(response, 'profile.html')

	def test_profile_contains_greeting(self):
		response = Client().get('/s6/profile/')
		response_content = response.content.decode('UTF-8')
		self.assertIn('Hi peeps!', response_content)

class Story6FunctionalTest(StaticLiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		service_log_path = "./chromedriver.log"
		service_args = ['--verbose']
		self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		self.browser.implicitly_wait(25) 
		super(Story6FunctionalTest,self).setUp()

	def tearDown(self):
		self.browser.quit()
		super(Story6FunctionalTest, self).tearDown()
	
	def test_add_status(self):
		self.browser.get('%s%s' % (self.live_server_url, '/s6/'))
		status_box = self.browser.find_element_by_name('content')
		status_box.send_keys('tes ah')
		status_box.submit()
		assert 'tes ah' in self.browser.page_source
		time.sleep(5)
