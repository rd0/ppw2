from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .forms import StatusForm
from .models import Status

# Create your views here.
def index(request):
	if request.method == 'POST':
		form = StatusForm(request.POST)
		if form.is_valid():
			status = Status()
			status.content = form.cleaned_data['content']
			status.save()
			return HttpResponseRedirect('/')
	else:
		response = {
			'status_list': Status.objects.all().order_by('-date'),
			'form': StatusForm
		}
		return render(request, 'index.html', response)

def profile(request):
	desc = ['Hi peeps! Welcome (again) to my profile, Currently undergraduate at Universitas Indonesia. ',
			'I enjoy travelling. Contact me via wazzap 081272977770 or line (id: yusufardho). Please visit ',
			'u-u.herokuapp.com for more :D thanks for your time.']
	desc = desc[0] + desc[1] + desc[2]
	response = {
		'desc': desc
	}
	return render(request, 'profile.html', response)
