function star(object) {
	var oldStar = $("#number").text()
	if (object.hasClass("fa-star-o")) {
		var tmp = parseFloat(oldStar) + 1
		object.removeClass("fa-star-o").addClass("fa-star").addClass("yellow-clr");
	} 
	else {
		var tmp = parseFloat(oldStar) - 1
		object.removeClass("fa-star").removeClass("yellow-clr").addClass("fa-star-o");
	}
	$("#number").text(tmp);
}

function getData(object, query) {
	$('#booklist').empty();
	// $('#number').text(0); // reset number of fav's
	$(function() {
		$.ajax({
			url: "/s9/" + query,
			success: function(result) {
			    $.each(result.items, function(idx, item) {
			        $('<tr>').append(
			        	$('<td>').text(idx+1),
			            $('<td>').text(item.volumeInfo.title),
			            $('<td>').text(item.volumeInfo.authors),
			            $('<td>').text(item.volumeInfo.publisher),
			            $('<td>').text(item.volumeInfo.publishedDate),
			            $('<td>').append('<div class="fav fa fa-star-o"></button>'),
			        ).appendTo('#booklist');
			    });
			}
		});
	});
}

$(document).ready(function() {
	getData($(this), "quilting"); // default query
	$('.btn-default').click(function() { getData($(this), $('#search-form').val()); });
	$("tbody").delegate(".fav", "click", function() { star($(this)); })
});