isValidName = false;
isValidEmail = false;
isValidPassword = false;

$(document).ready(function() {
	$(".alert").hide();
	$('#id_name').on('input focusout', function(){
	    validateName();
	    toggleButton();
	});

	$('#id_email').on('input focusout', function(){
	    validateEmail();
	    toggleButton();
	});

	$('#id_password').on('input focusout', function(){
	    validatePassword();
	    toggleButton();
	});

	$('form').submit(function(event) {
        var formData = $(this).serializeArray();
        $.ajax({
            type: 'POST',
            url: 'add_user_as_subscriber',
            data: formData,
            dataType: 'json',
        }).done(function(response) {
            $(".alert").hide();
            $(".alert-success").show();
        }).fail(function(response) {
            $(".alert").hide();
            $(".alert-danger").show();
        });
        event.preventDefault();
        resetForm();
    });
});

function resetForm(){
	$(':input','#subscribe-form')
	.not(':button, :submit, :reset, :hidden')
	.val('')
	.prop('checked', false)
	.prop('selected', false);

	isValidName = false;
	isValidEmail = false;
	isValidPassword = false;

    deactivateButton();
}

function toggleButton(){
	if (isValidName && isValidPassword && isValidEmail) activateButton();
	else deactivateButton();
}

function activateButton(){
	$('#submit_button').prop('disabled', false);
	$('#submit_button').removeClass('disabled');
	$('#submit_butoon').addClass('btn-info');
}

function deactivateButton(){
	$('#submit_button').prop('disabled', true);
}

function validateName(){
	var nameLength = $('#id_name').val().length;
	if (nameLength === 0){
		isValidName = false;
		$("#name_errors").html("<li>Name can't be empty</li>");
		return;
	}

	if (nameLength > 50){
		isValidName = false;
		$("#name_errors").html("<li>Name exceeds 50 character</li>");
		return;
	}
	$("#name_errors").empty();
	isValidName = true;
}

function validatePassword(){
	var passwordLength = $('#id_password').val().length;

	if (passwordLength === 0){
		isValidPassword = false;
		$("#password_errors").html("<li>Password can't be empty</li>");
		return;
	}

	if (passwordLength < 8){
		isValidPassword = false;
		$("#password_errors").html("<li>Password must be 8 characters or more</li>");
		return;
	}

	if (passwordLength > 64){
		isValidPassword = false;
		$("#password_errors").html("<li>Password exceeds 64 character</li>");
		return;
	}
	$("#password_errors").empty();
	isValidPassword = true;
}

function validateEmail(){
	email = $('#id_email').val()
	return $.ajax({
        type        : 'GET',
        url         : 'isEmailValid?email=' + email,
        dataType    : 'json',
    }).then(function(response) {
    	$('#email_errors').empty();
        if (response.valid && !response.exists) {
            isValidEmail = true;
        } 
        else {
        	var emailErrors = "";
            $.each(response.messages, function(i, message) {
				emailErrors = $("<li>").append(message);
            });
            emailErrors.appendTo($("#email_errors"));
            isValidEmail = false;
        }
	    toggleButton();
    });
}
