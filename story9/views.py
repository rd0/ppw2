from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.contrib.auth import logout as logout_fun

import json
import requests


# Create your views here.
def index(request):
	return render(request, 'index-s9.html')

def getJSON(request, query):
	url = 'https://www.googleapis.com/books/v1/volumes?q=' + query
	data = json.loads(requests.get(url).text)
	return JsonResponse(data)

def logout(request):
	logout_fun(request)
	return redirect('../../s9/')

