from django.urls import path
from . import views

app_name = 's9'

urlpatterns = [
    path('', views.index, name='home'),
    path('logout/', views.logout),
    path('<str:query>/', views.getJSON),
]