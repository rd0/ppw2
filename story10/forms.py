from django import forms
from .models import User

class RegUserForm(forms.ModelForm):
	name = forms.CharField(max_length=50, widget=forms.TextInput(attrs={
		'class': 'form-control',
		'placeholder': 'name ...'
	}))

	email = forms.CharField(max_length=255, widget=forms.EmailInput(attrs={
		'class': 'form-control',
		'placeholder': 'email ...'
	}))

	password = forms.CharField(max_length=64, widget=forms.PasswordInput(attrs={
		'class': 'form-control',
		'placeholder': 'password ...'
	}))

	class Meta:
		model = User
		fields = '__all__'