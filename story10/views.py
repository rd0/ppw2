from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.core.validators import validate_email 
from django.core.exceptions import ValidationError

from .models import User
from .forms import RegUserForm

# Create your views here.
def index(request):
	response = {'RegisterFormAsSubscriber' : RegUserForm}
	return render(request, 'index-s10.html', response)

def add_user_as_subscriber(request):
	form = RegUserForm(request.POST)
	if form.is_valid():
		form.save()
		return JsonResponse({'success': True})
	else: # form_is'nt_valid
		response = JsonResponse({'success': False, 'errors': form.errors})
		response.status_code = 403
		return response

def isEmailValid(request):
	email = request.GET.get('email', '')
	response = {'valid': None, 'exists': None, 'messages': []}
	try:
		validate_email(email)
	except ValidationError as error:
		response['valid'] = False
		response['messages'] = error.messages
	else:
		response['valid'] = True
		if User.objects.filter(email=email):
			response['exists'] = True
			response['messages'] = ["User with this email already registered"]
		else:
			response['exist'] = False
	return JsonResponse(response)