from django.urls import path
from django.conf.urls import url
from . import views

app_name = 's10'

urlpatterns = [
	path('', views.index),
	path('add_user_as_subscriber', views.add_user_as_subscriber),
	path('isEmailValid', views.isEmailValid),
]
