from django.db import models
from django.core.validators import MinLengthValidator

# Create your models here.
class User(models.Model):
	name = models.CharField(max_length=50)
	email = models.EmailField(max_length=255, unique=True)
	password = models.CharField(max_length=64, validators=[MinLengthValidator(8)])